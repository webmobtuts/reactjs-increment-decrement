import IncrementDecrement from "./IncrementDecrement";

function App() {
  return (
    <div className="App">
      <IncrementDecrement min={10} max={30} step={2} />
    </div>
  );
}

export default App;
